TARGET?=	book
BIBFILE?=	book.bib
NOMFILE?=	book.nlo

TARGETFILES?=	book.tex \
				preamble.tex \
				settings.tex \
				layout.tex \
				vars.tex \
				lib/*.tex \
				poetry/*.tex \
				prose/*.tex \
				tech/*.tex \
				titles.tex

default: pdf-fast

include include/latex.mk
